from flask import Flask, jsonify
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from config import Config
from db import db, migrate

from resources.lotto import Lotto
from resources.eurojackpot import Eurojackpot
from resources.user import UserLogin, TokenRefresh
from resources.course import Course, CourseList
from resources.section import Section
from resources.word import Word
from resources.translation import Translation, TranslationList
from resources.alternative import Alternative, AlternativeList
from resources.multiword import MultiWord
from resources.ipaddress import IpAddress

app = Flask(__name__)
app.config.from_object(Config)
db.init_app(app)
migrate.init_app(app, db)
api = Api(app)
jwt = JWTManager(app)

@jwt.expired_token_loader
def expired_token_callback():
    return jsonify({
        'description': 'The token has expired',
        'error': 'token_expired'
    }), 401

@jwt.invalid_token_loader
def invalid_token_callback(error):
    return jsonify({
        'description': 'Signature verification failed.',
        'error': 'invalid_token'
    }), 401

@jwt.unauthorized_loader
def unauthorized_loader_callback(error):
    return jsonify({
        'description': 'Request does not contain an access token.',
        'error': 'authorization_required'
    }), 401

@jwt.needs_fresh_token_loader
def needs_fresh_token_loader_callback():
    return jsonify({
        'description': 'The token is not fresh.',
        'error': 'fresh_token_required'
    }), 401

@jwt.revoked_token_loader
def revoked_token_loader_callback():
    return jsonify({
        'description': 'The token has been revoked.',
        'error': 'token_revoked'
    }), 401

cors = CORS(app, resources={r'/auth/*': {'origins': '*'}, r'/onoki/*': {'origins': '*'}})

api.add_resource(Lotto, '/lotto')
api.add_resource(Eurojackpot, '/eurojackpot')
api.add_resource(UserLogin, '/auth')
api.add_resource(TokenRefresh, '/auth/refresh')
api.add_resource(Course, '/onoki/course/<int:_id>', '/onoki/course')
api.add_resource(CourseList, '/onoki/courses/<int:user_id>', '/onoki/courses')
api.add_resource(Section, '/onoki/section/<int:_id>', '/onoki/section')
api.add_resource(Word, '/onoki/word/<int:_id>', '/onoki/word')
api.add_resource(Translation, '/onoki/translation/<int:_id>', '/onoki/translation')
api.add_resource(TranslationList, '/onoki/translations')
api.add_resource(Alternative, '/onoki/alternative/<int:_id>', '/onoki/alternative')
api.add_resource(AlternativeList, '/onoki/alternatives')
api.add_resource(MultiWord, '/onoki/word/multiple/<int:section_id>')
api.add_resource(IpAddress, '/ip')

if __name__ == '__main__':
    app.run(port=5000, debug=True)