import os
from datetime import timedelta
from settings import secret_key, db_user, db_pass, db_name
basedir = os.path.abspath(os.path.dirname(__file__))
expires_in_minutes = 60

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or secret_key
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'postgresql://{}:{}@localhost/{}'.format(db_user, db_pass, db_name)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = True
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=expires_in_minutes)