from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_jwt_identity
from models.section import SectionModel
from models.course import CourseModel
from models.user import UserModel

class Section(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'name',
        type=str,
        required=True,
        help='This is a required field.'
    )
    parser.add_argument(
        'course_id',
        type=int,
        required=True,
        help='This is a required field.'
    )
    put_parser = reqparse.RequestParser()
    put_parser.add_argument(
        'name',
        type=str,
        required=True,
        help='This is a required field.'
    )

    def get(self, _id):
        section = SectionModel.find_by_id(_id)
        if section:
            return section.json()
        return {"message": "No section found with the id: {}".format(_id)}, 404

    @jwt_required
    def post(self):
        user_id = get_jwt_identity()
        user = UserModel.find_by_id(user_id)

        data = Section.parser.parse_args()
        course = CourseModel.find_by_id(data['course_id'])
        if not course:
            return {"message": "Invalid course id."}, 400
        elif not course.user_id == user_id and not user.is_admin:
            return {"message": "You are not authorized to modify this course."}, 401
        if SectionModel.query.filter_by(course_id=data['course_id'], name=data['name']).first():
            return {"message": "The course already has a section named '{}'.".format(data['name'])}, 400
        section = SectionModel(data['name'], data['course_id'])
        try:
            section.save_to_db()
        except:
            return {"message": "A database error occurred."}, 500
        return section.json()
    
    @jwt_required
    def put(self, _id):
        user_id = get_jwt_identity()
        user = UserModel.find_by_id(user_id)
        data = Section.put_parser.parse_args()
        section = SectionModel.find_by_id(_id)
        if not section:
            return {'message': 'No section found with the id {}'.format(_id)}
        course = CourseModel.find_by_id(section.course_id)
        if not course.user_id == user_id and not user.is_admin:
            return {'message': 'You are not authorized to modify this section.'}, 401
        section.name = data['name']
        try:
            section.save_to_db()
        except:
            return {'message': 'A database error occurred.'}, 500
        return section.json()
    
    @jwt_required
    def delete(self, _id):
        user_id = get_jwt_identity()
        user = UserModel.find_by_id(user_id)
        section = SectionModel.find_by_id(_id)
        if section:
            course = CourseModel.find_by_id(section.course_id)
            try:
                if not user_id == course.user_id and not user.is_admin:
                    return {'message': 'You are not authorized to delete this section.'}, 401
                section.delete_from_db()
            except:
                return {"message": "A database error occurred."}, 500
        return {"message": "Section deleted."}