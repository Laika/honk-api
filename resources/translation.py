from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_jwt_identity
from models.translation import TranslationModel
from models.word import WordModel
from resources.word import Word

class Translation(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'word_id',
        type=int,
        required=True,
        help='This is a required field.'
    )
    parser.add_argument(
        'translation',
        type=str,
        required=True,
        help='This is a required field.'
    )

    def get(self, _id):
        translation = TranslationModel.find_by_id(_id)
        if translation:
            return translation.json()
        return {"message": "No translation found with the id: {}".format(_id)}
    
    @jwt_required
    def post(self):
        data = Translation.parser.parse_args()
        user_id = get_jwt_identity()
        word = WordModel.find_by_id(data['word_id'])
        if not word:
            return {"message": "Invalid word id."}, 400
        if not Word.is_authorized(user_id, word.section_id):
            return {"message": "You are not authorized to modify this section."}, 401
        translation = TranslationModel(**data)
        try:
            translation.save_to_db()
        except:
            return {"message": "A database error occurred."}, 500
        return translation.json()
    
    @jwt_required
    def delete(self, _id):
        user_id = get_jwt_identity()
        translation = TranslationModel.find_by_id(_id)
        word = WordModel.find_by_id(translation.word_id)
        if not Word.is_authorized(user_id, word.section_id):
            return {"message": "You are not authorized to modify this section."}, 401
        if translation:
            try:
                translation.delete_from_db()
            except:
                return {"message": "A database error occurred."}, 500
        return {"message": "Translation deleted."}
    
    @jwt_required
    def put(self, _id):
        user_id = get_jwt_identity()
        data = Translation.parser.parse_args()
        translation = TranslationModel.find_by_id(_id)
        word = WordModel.find_by_id(data['word_id'])
        if not word:
            return {"message": "Invalid word id."}, 400
        if not Word.is_authorized(user_id, word.section_id):
            return {"message": "You are not authorized to modify this section."}, 401
        if translation:
            translation.translation = data['translation']
        else:
            translation = TranslationModel(data['word_id'], data['translation'])
        try:
            translation.save_to_db()
        except:
            return {"message": "A database error occurred."}, 500
        return translation.json()


class TranslationList(Resource):
    @jwt_required
    def get(self):
        return {
            "translations": [t.json() for t in TranslationModel.query.all()]
        }
