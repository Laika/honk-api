from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_jwt_identity
from models.word import WordModel
from models.section import SectionModel
from models.course import CourseModel
from models.user import UserModel

class Word(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'word',
        type=str,
        required=True,
        help='This is a required field.'
    )
    parser.add_argument(
        'section_id',
        type=int,
        required=True,
        help='This is a required field.'
    )
    def get(self, _id):
        word = WordModel.find_by_id(_id)
        if word:
            return word.json()
        return {"message": "No word found with the id: {}".format(_id)}, 404
    
    @classmethod
    def is_authorized(cls, user_id, section_id):
        user = UserModel.find_by_id(user_id)
        section = SectionModel.find_by_id(section_id)
        course = CourseModel.find_by_id(section.course_id)
        if not user.is_admin and not user_id == course.user_id:
            return False
        return True
    
    @jwt_required
    def post(self):
        data = Word.parser.parse_args()
        user_id = get_jwt_identity()

        if not SectionModel.find_by_id(data['section_id']):
            return {"message": "Invalid section id."}, 400
        
        if not self.is_authorized(user_id, data['section_id']):
            return {"message": "You are not authorized to modify this section."}, 401

        word = WordModel(**data)

        try:
            word.save_to_db()
        except:
            return {"message": "A database error occurred."}, 500
        return word.json()
    
    @jwt_required
    def delete(self, _id):
        word = WordModel.find_by_id(_id)
        if not word:
            return {"message": "Word deleted."}
        user_id = get_jwt_identity()
        if not self.is_authorized(user_id, word.section_id):
            return {"message": "You are not authorized to modify this section."}, 401
        try:
            word.delete_from_db()
        except:
            return {"message": "A database error occurred."}, 500
        return {"message": "Word deleted."}
    
    @jwt_required
    def put(self, _id):
        data = Word.parser.parse_args()
        word = WordModel.find_by_id(_id)
        user_id = get_jwt_identity()
        if not self.is_authorized(user_id, data['section_id']):
            return {"message": "You are not authorized to modify this section"}, 401
        if word:
            word.word = data['word']
        else:
            word = WordModel(**data)
        try:
            word.save_to_db()
        except:
            return {"message": "A database error occurred."}, 500
        return word.json()