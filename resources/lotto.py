from random import shuffle
from flask_restful import Resource

class Lotto(Resource):
    def get(self):
        pool = [x for x in range(1, 41)]
        row = []
        shuffle(pool)
        for i in range(7):
            row.append(pool.pop())
        return {
            "row": row
        }