from flask_restful import Resource
from flask import request

class IpAddress(Resource):
    def get(self):
        return {
            'ip': request.remote_addr
        }