from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_jwt_identity
from models.course import CourseModel
from models.user import UserModel

class Course(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'name',
        type=str,
        required=True,
        help='This is a required field.'
    )
    parser.add_argument(
        'target_language',
        type=str,
        required=True,
        help='This is a required field.'
    )
    parser.add_argument(
        'language',
        type=str,
        required=True,
        help='This is a required field.'
    )
    put_parser = reqparse.RequestParser()
    put_parser.add_argument(
        'name',
        type=str,
        required=False
    )
    put_parser.add_argument(
        'language',
        type=str,
        required=False
    )
    put_parser.add_argument(
        'target_language',
        type=str,
        required=False
    )
    def get(self, _id):
        course = CourseModel.find_by_id(_id)
        if course:
            return course.json()
        return {"message": "No course found with the id: {}".format(_id)}, 404
    
    @jwt_required
    def post(self):
        user_id = get_jwt_identity()
        if not UserModel.find_by_id(user_id):
            return {'message': 'Something went wrong.'}, 500

        data = Course.parser.parse_args()

        if CourseModel.query.filter_by(name=data['name'], user_id=user_id).first():
            return {"message": "You have already created a course called '{}'".format(data['name'])}, 400

        course = CourseModel(data['name'], user_id, data['target_language'], data['language'])
        try:
            course.save_to_db()
        except:
            return {"message": "A database error occurred."}, 500
        return course.json()
    
    @jwt_required
    def delete(self, _id):
        user_id = get_jwt_identity()
        user = UserModel.find_by_id(user_id)
        if not user:
            return {'message': 'Something went wrong.'}, 400
        course = CourseModel.find_by_id(_id)
        if not user.is_admin and not user_id == course.user_id:
            return {'message': 'Courses can only be deleted by their creator or an admin.'}, 401
        if course:
            try:
                course.delete_from_db()
            except:
                return {"message": "A database error occurred."}, 500
        return {"message": "Course deleted."}
    
    @jwt_required
    def put(self, _id):
        user_id = get_jwt_identity()
        user = UserModel.find_by_id(user_id)
        course = CourseModel.find_by_id(_id)
        if not course:
            return {'message': 'No course found with the id {}'.format(_id)}
        if not user.is_admin and not user_id == course.user_id:
            return {'message': 'You are not authorized to edit this course.'}, 401
        data = Course.put_parser.parse_args()
        if data['name']:
            course.name = data['name']
        if data['language']:
            course.lang = data['language']
        if data['target_language']:
            course.target_lang = data['target_language']
        
        try:
            course.save_to_db()
        except:
            return {'message': 'A database error occurred.'}, 500
        return course.json()


class CourseList(Resource):
    def get(self, user_id=None):
        if not user_id:
            return {'courses': [course.json() for course in CourseModel.query.order_by(CourseModel.id).all()]}
        courses = CourseModel.query.order_by(CourseModel.id).filter_by(user_id=user_id).all()
        if courses:
            return {
                'courses': [course.json() for course in courses]
            }