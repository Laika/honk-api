from flask import current_app
from flask_restful import Resource, reqparse
from flask_jwt_extended import (
    create_access_token, 
    create_refresh_token, 
    jwt_refresh_token_required,
    get_jwt_identity
)
from models.user import UserModel
from config import expires_in_minutes

class User(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'username',
        type=str,
        required=True,
        help='This is a required field.'
    )
    parser.add_argument(
        'email',
        type=str,
        required=True,
        help='This is a required field.'
    )
    parser.add_argument(
        'password',
        type=str,
        required=True,
        help='This is a required field.'
    )
    def get(self, _id):
        user = UserModel.find_by_id(_id)
        if user:
            return user.json()
        return {"message": "No user found with the id: {}".format(_id)}
    
    def post(self):
        data = User.parser.parse_args()
        if UserModel.find_by_username(data['username']):
            return {"message": "A user with the user name '{}' already exists."}, 400
        if UserModel.find_by_email(data['email']):
            return {"message": "A user with the email '{}' already exists."}, 400
        user = UserModel(data['username'], data['email'], data['password'])
        try:
            user.save_to_db()
        except:
            return {"message": "A database error occurred."}, 500
        return user.json()
    
    def delete(self, _id):
        user = UserModel.find_by_id(_id)
        if user:
            try:
                user.delete_from_db()
            except:
                return {"message": "A database error occurred."}, 500
        return {"message": "User deleted."}

class UserList(Resource):
    def get(self):
        return {
            'users': [u.json() for u in UserModel.query.all()]
        }


class UserLogin(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'username',
        type=str,
        required=True,
        help='This is a required field.'
    )
    parser.add_argument(
        'password',
        type=str,
        required=True,
        help='This is a required field.'
    )

    @classmethod
    def post(cls):
        data = cls.parser.parse_args()

        user = UserModel.find_by_username(data['username'])

        if user and user.check_password(data['password']):
            access_token = create_access_token(identity=user.id, fresh=True)
            refresh_token = create_refresh_token(identity=user.id)
            return {
                'access_token': access_token,
                'refresh_token': refresh_token,
                'expires_in': expires_in_minutes * 60
            }, 200
        return {
            'message': 'Invalid credentials.'
        }, 401


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return {
            'access_token': new_token,
            'expires_in': expires_in_minutes * 60
        }, 200