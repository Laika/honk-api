from random import shuffle
from flask_restful import Resource

class Eurojackpot(Resource):
    def get(self):
        pool = [x for x in range(1, 51)]
        pool_s = [x for x in range(1, 11)]
        row = []
        row_s = []
        shuffle(pool)
        shuffle(pool_s)
        for i in range(5):
            row.append(pool.pop())
        for i in range(2):
            row_s.append(pool_s.pop())
        return {
            "row": row,
            "star_numbers": row_s
        }