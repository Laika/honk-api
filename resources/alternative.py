from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_jwt_identity
from models.alternative import AlternativeModel
from models.word import WordModel
from resources.word import Word

class Alternative(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'word_id',
        type=int,
        required=True,
        help='This is a required field.'
    )
    parser.add_argument(
        'alternative',
        type=str,
        required=True,
        help='This is a required field.'
    )

    def get(self, _id):
        alternative = AlternativeModel.find_by_id(_id)
        if alternative:
            return alternative.json()
        return {"message": "No alternative found with the id: {}".format(_id)}
    
    @jwt_required
    def post(self):
        data = Alternative.parser.parse_args()
        user_id = get_jwt_identity()
        word = WordModel.find_by_id(data['word_id'])
        if not word:
            return {"message": "Invalid word id"}, 400
        if not Word.is_authorized(user_id, word.section_id):
            return {"message": "You are not authorized to modify this section."}, 401
        alternative = AlternativeModel(data['word_id'], data['alternative'])
        try:
            alternative.save_to_db()
        except:
            return {"message": "A database error occurred."}, 500
        return alternative.json()

    @jwt_required
    def delete(self, _id):
        user_id = get_jwt_identity()
        alternative = AlternativeModel.find_by_id(_id)
        word = WordModel.find_by_id(alternative.word_id)
        if not Word.is_authorized(user_id, word.section_id):
            return {"message": "You are not authorized to modify this section."}, 401
        if alternative:
            try:
                alternative.delete_from_db()
            except:
                return {"message": "A database error occurred."}, 500
        return {"message": "Alternative deleted"}
    
    @jwt_required
    def put(self, _id):
        user_id = get_jwt_identity()
        data = Alternative.parser.parse_args()
        alternative = AlternativeModel.find_by_id(_id)
        word = WordModel.find_by_id(data['word_id'])
        if not word:
            return {"message": "Invalid word id."}, 400
        if not Word.is_authorized(user_id, word.section_id):
            return {"message": "You are not authorized to modify this section."}, 401
        if alternative:
            alternative.word = data['alternative']
        else:
            alternative = AlternativeModel(data['word_id'], data['alternative'])
        try:
            alternative.save_to_db()
        except:
            return {"message": "A database error occurred."}, 500
        return alternative.json()

class AlternativeList(Resource):
    @jwt_required
    def get(self):
        return {"alternatives": [a.json() for a in AlternativeModel.query.all()]}