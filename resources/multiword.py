from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
from models.word import WordModel
from models.translation import TranslationModel
from models.section import SectionModel
from resources.word import Word

class MultiWord(Resource):
    @jwt_required
    def post(self, section_id):
        if not SectionModel.find_by_id(section_id):
            return {'message': 'invalid section id'}, 400
        user_id = get_jwt_identity()
        if not Word.is_authorized(user_id, section_id):
            return {'message': 'You are not authorized to modify this section.'}, 401

        data = request.data.decode('utf-8')
        data = data.replace('\r', '')
        lines = data.split("\n")
        words = []
        translations = []
        try:
            for line in lines:
                split_line = line.split(":")
                words.append(split_line[0])
                translations.append(split_line[1])
        except:
            return {"message": "Bits went sideways!"}, 500

        if len(words) == len(translations):

            for i in range(len(words)):
                w = WordModel(words[i], section_id)
                try:
                    w.save_to_db()
                    for t in translations[i].split(";"):
                        trans = TranslationModel(w.id, t)
                        trans.save_to_db()
                except:
                    return {'message': 'fucking hell'}

        return {
            'message': '{} words successfully created.'.format(len(words))
        }