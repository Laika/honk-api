from db import db

class AlternativeModel(db.Model):
    __tablename__ = 'alternatives'

    id = db.Column(db.Integer, primary_key=True)
    word_id = db.Column(db.Integer, db.ForeignKey('words.id'))
    word = db.Column(db.String(80))

    def __init__(self, word_id, word):
        self.word_id = word_id
        self.word = word

    def json(self):
        return {
            "id": self.id,
            "word": self.word
        }
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()