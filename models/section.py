from db import db
from models.word import WordModel

class SectionModel(db.Model):
    __tablename__ = 'sections'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(124))
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'))
    course = db.relationship('CourseModel')
    words = db.relationship('WordModel', cascade='all, delete-orphan', lazy='dynamic')

    def __init__(self, name, course_id):
        self.name = name
        self.course_id = course_id

    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "course": self.course.name,
            "words": [word.json() for word in self.words.order_by(WordModel.id).all()]
        }
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()