from db import db

class TranslationModel(db.Model):
    __tablename__ = 'translations'

    id = db.Column(db.Integer, primary_key=True)
    word_id = db.Column(db.Integer, db.ForeignKey('words.id'))
    translation = db.Column(db.String(80))

    def __init__(self, word_id, translation):
        self.word_id = word_id
        self.translation = translation
    
    def json(self):
        return {
            "id": self.id,
            "translation": self.translation
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()