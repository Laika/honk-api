from db import db
from models.translation import TranslationModel
from models.alternative import AlternativeModel

class WordModel(db.Model):
    __tablename__ = 'words'

    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.String(80))
    section_id = db.Column(db.Integer, db.ForeignKey('sections.id'))
    note = db.Column(db.String(255), default='')
    alternatives = db.relationship('AlternativeModel', cascade='all, delete-orphan', lazy='dynamic')
    translations = db.relationship('TranslationModel', cascade='all, delete-orphan', lazy='dynamic')

    def __init__(self, word, section_id):
        self.word = word
        self.section_id = section_id

    def json(self):
        return {
            "id": self.id,
            "word": self.word,
            "note": self.note,
            "alternatives": [alternative.json() for alternative in self.alternatives.order_by(AlternativeModel.id).all()],
            "translations": [translation.json() for translation in self.translations.order_by(TranslationModel.id).all()]
        }
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()