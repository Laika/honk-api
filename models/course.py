from datetime import datetime
from db import db
from models.section import SectionModel

class CourseModel(db.Model):
    __tablename__ = 'courses'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    created_by = db.relationship('UserModel', foreign_keys='CourseModel.user_id')
    created = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    target_lang = db.Column(db.String(64))
    lang = db.Column(db.String(64))
    sections = db.relationship('SectionModel', cascade='all, delete-orphan', lazy='dynamic')

    def __init__(self, name, user_id, target_lang, lang):
        self.name = name
        self.user_id = user_id
        self.target_lang = target_lang
        self.lang = lang
    
    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "created_by": self.created_by.username,
            "created": str(self.created),
            "target_language": self.target_lang,
            "language": self.lang,
            "sections": [{"id": section.id, "name": section.name} for section in self.sections.order_by(SectionModel.id).all()]
        }
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()